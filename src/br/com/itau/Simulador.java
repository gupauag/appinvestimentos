package br.com.itau;

import java.text.DecimalFormat;

public class Simulador {

    public static String calculaJurosCompostos(Investidor invest) {
        DecimalFormat decimal = new DecimalFormat( "0.00" );
        //juros compostos = valor * (1 + taxa)^tempo;
        return decimal.format(invest.getValor() * (Math.pow((1 + invest.getTaxaRendimento()), invest.getTempo())));
    }

}
