package br.com.itau;

import static br.com.itau.Impressora.imprimeProjecao;

public class Main {
    public static void main (String[] args) throws InterruptedException {

        Investidor investidor = new Investidor();
        Simulador simulador = new Simulador();
        Taxas tax = new Taxas();

        investidor = Impressora.inputDados();
        Impressora.aguarde(3000); //aguarda 3 segundos
        investidor.setTaxaRendimento(tax.getTaxadefault());

        imprimeProjecao(investidor, simulador.calculaJurosCompostos(investidor));

    }
}
