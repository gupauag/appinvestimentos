package br.com.itau;

import java.util.Scanner;

public class Impressora {

    public static Investidor inputDados() {

        Scanner entradaConsole = new Scanner(System.in);
        Investidor investidor = new Investidor();

        System.out.print("Olá. Digite seu nome: ");
        investidor.setNome(entradaConsole.next());
        System.out.println("Olá " + investidor.getNome() + ". Bem vindo ao App de Investimentos.");
        System.out.println("Observação. Nosso investimento remunera 0,7% ao mês!");
        System.out.println("Para dar sequencia na simulação de investimentos, informe os seguintes dados:");
        System.out.print("Valor à investir: ");
        investidor.setValor(entradaConsole.nextDouble());
        System.out.print("Tempo de investimento (em meses): ");
        investidor.setTempo(entradaConsole.nextInt());

        return investidor;
    }

    public static void imprimeProjecao(Investidor invest, String projecao){
        StringBuilder impressao = new StringBuilder();
        impressao.append("\nPronto! Obrigado por aguardar, sr(a): "+invest.getNome()+ "\n");
        impressao.append("Seu investimento, renderá R$ "+projecao);
        if (invest.getTempo() == 1) {
            impressao.append(" em " + invest.getTempo() + " mês");
        }else {
            impressao.append(" em " + invest.getTempo() + " mêses");
        }
        impressao.append(" a uma taxa de " + invest.getTaxaRendimento() + "%");
        System.out.println(impressao.toString());
    }

    public static void aguarde(int timer) throws InterruptedException {
        System.out.print("Calculando retorno esperado... aguarde!");
        new Thread().sleep(timer);
    }
}
