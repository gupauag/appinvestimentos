package br.com.itau;

public class Investidor {

    private String nome;
    private double valor;
    private int tempo;
    private double taxaRendimento;


    public Investidor() {
        this.nome = "";
        this.valor = 0.00;
        this.tempo = 0;
        this.taxaRendimento = 0.00;
    }

    public double getTaxaRendimento() {
        return taxaRendimento;
    }

    public void setTaxaRendimento(double taxaRendimento) {
        this.taxaRendimento = taxaRendimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getTempo() {
        return tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }
}
